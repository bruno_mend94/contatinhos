package br.com.brunoazevedo.contatinhos

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_lista_contatinhos.*
import java.util.ArrayList

class ListaContatinhosActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CADASTRO: Int = 1 //para executar o cadastro de contatinho
        private const val LISTA = "ListaContatinhos" //para salvar e restaurar a lista quando necessário
    }

    //lista para armazenar contatinhos adicionados
    var listaContatinhos: MutableList<Contatinho> = mutableListOf()
    //indice para verificar se algum contatinho foi clicado
    var indexContatinhoClicado: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_contatinhos)

        //abre a activity de cadastro de contatinho e aguarda um resultado para adicioná-lo na lista
        btnAddContatinho.setOnClickListener(){
            val cadastrarContatinho = Intent(this,CadastraContatinhoActivity::class.java)
            startActivityForResult(cadastrarContatinho, REQUEST_CADASTRO)
        }
    }

    //carrega a lista sempre que a activity é atualizada
    override fun onResume() {
        super.onResume()
        carregaLista()
    }

    //recebe o contatinho da tela de cadastro e o adiciona na lista
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == REQUEST_CADASTRO && resultCode == Activity.RESULT_OK){
            val contatinho: Contatinho? = data?.getSerializableExtra(CadastraContatinhoActivity.CONTATINHO) as Contatinho
            //caso algum item tenha sido clicado seus dados são alterados, caso não adiciona um novo
            if (contatinho != null) {
                if(indexContatinhoClicado >= 0){
                    listaContatinhos.set(indexContatinhoClicado, contatinho)
                    indexContatinhoClicado = -1
                }else {
                    listaContatinhos.add(contatinho)
                }
            }
        }

    }

    //salva a lista caso o Android venha a destruir a activity
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putSerializable(LISTA, listaContatinhos as ArrayList<String>)
    }

    //restaura a lista caso o Android venha a destruir a activity
    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        if(savedInstanceState != null)
            listaContatinhos = savedInstanceState.getSerializable(LISTA) as MutableList<Contatinho>
    }

    //configura os componentes necesário para utilizar a RecyclerView
    fun carregaLista() {
        val adapter = ContatinhoAdapter(this, listaContatinhos)

        //configura o clique em cada item do RecyclerView
        adapter.setOnItemClickListener { contatinho, indexContatinhoClicado ->
            this.indexContatinhoClicado = indexContatinhoClicado
            val editaContatinho = Intent(this, CadastraContatinhoActivity::class.java)
            editaContatinho.putExtra(CadastraContatinhoActivity.CONTATINHO, contatinho)
            this.startActivityForResult(editaContatinho, REQUEST_CADASTRO)
        }

        val layoutManager = LinearLayoutManager(this)

        rvContatinhos.adapter = adapter
        rvContatinhos.layoutManager = layoutManager
    }

}
