package br.com.brunoazevedo.contatinhos

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.contatinho_item_lista.view.*

class ContatinhoAdapter(val context: Context, val contatinhos: List<Contatinho>)
    : RecyclerView.Adapter<ContatinhoAdapter.ViewHolder>() {

    //salva a função do clique no item
    var clickListener: ((contatinho:Contatinho, index: Int) -> Unit)? = null

    //método responsável por inflar as views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.contatinho_item_lista, parent, false)
        return ViewHolder(view)
    }

    //retorna a quantidade de itens na lista
    override fun getItemCount(): Int {
        return contatinhos.size
    }

    //popula o ViewHolder com as informações do contatinho
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(context, contatinhos[position], clickListener)
    }

    //configuração a função de clique nos itens
    fun setOnItemClickListener(clique: ((contatinho:Contatinho, index: Int) -> Unit)){
        this.clickListener = clique
    }

    //referência para a view de cada item da lista
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(context:Context, contatinho: Contatinho, clickListener: ((contatinho:Contatinho, index: Int) -> Unit)?) {
            itemView.tvNome.text = contatinho.nome
            itemView.tvTelefone.text = contatinho.telefone

            val thumbnail = GlideApp.with(context)
                    .load(R.drawable.ic_person)
                    .apply(RequestOptions().circleCrop())

            GlideApp.with(context)
                    .load(contatinho.caminhoFoto)
                    .thumbnail(thumbnail)
                    .centerCrop()
                    .apply(RequestOptions().circleCrop())
                    .into(itemView.imgFoto)

            if(clickListener != null) {
                itemView.setOnClickListener {
                    clickListener.invoke(contatinho, adapterPosition)
                }
            }

        }

    }
}