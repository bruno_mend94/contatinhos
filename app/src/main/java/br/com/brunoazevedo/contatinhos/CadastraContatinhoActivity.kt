package br.com.brunoazevedo.contatinhos

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_cadastro.*
import java.io.File

class CadastraContatinhoActivity : AppCompatActivity() {

    companion object {
        public const val CONTATINHO: String = "Contatinho" //para putExtra entre activities
        private const val REQUEST_PESMISSOES: Int = 3 //para solicitar permissão de ligação em tempo real
        private const val REQUEST_CAMERA: Int = 10 //para obter resposta do aplicativo de câmera
    }

    var caminhoFoto:String? = null //salva o caminho da foto tirada
    var caminhoFotoAceita:String? = null //salva o caminho da foto aceita pelo usuário

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastro)

        //verifica se foi passado um contatinho para alteração
        val contatinho: Contatinho? = intent.getSerializableExtra(CONTATINHO) as Contatinho?
        if(contatinho != null){
            carregaDados(contatinho)
        }

        btnFoto.setOnClickListener(){
            tirarFoto()
        }

        imgTelefone.setOnClickListener{ view ->
            telefonaOuEnviaMensagem(view)
        }

        imgEmail.setOnClickListener{
            enviaEmail()
        }

        imgEndereco.setOnClickListener{
            mostraNoMapa()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_cadastro, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.menuSalvar -> salvaContatinho()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        //caso não seja concedida permissão para ligação exibe uma mensagem
        if(requestCode == REQUEST_PESMISSOES && (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED)){
            Toast.makeText(this, getString(R.string.permissao_ligacao), Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //exibe a foto retornada pelo aplicativo de câmera
        if(requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK){
            GlideApp.with(this)
                    .load(caminhoFoto)
                    .placeholder(R.drawable.ic_person)
                    .centerCrop()
                    .into(imgFoto)

            caminhoFotoAceita = caminhoFoto
        }

    }

    //cria uma intent implícita para executar algum app de câmera que retorne a foto
    private fun tirarFoto() {
        val tirarFoto = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if (tirarFoto.resolveActivity(packageManager) != null) {
            val arquivoFoto = montaArquivoFoto()
            val uriFoto = FileProvider.getUriForFile(this, "${BuildConfig.APPLICATION_ID}.fileprovider", arquivoFoto)
            tirarFoto.putExtra(MediaStore.EXTRA_OUTPUT, uriFoto)
            startActivityForResult(tirarFoto, REQUEST_CAMERA)
        } else {
            Toast.makeText(this, "Impossível tirar foto", Toast.LENGTH_SHORT).show()
        }
    }

    //cria um arquivo temporário com nome único para salvar a foto
    private fun montaArquivoFoto(): File {
        val nomeArquivo = System.currentTimeMillis().toString()
        val diretorioArquivo = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val arquivoFoto = File.createTempFile(nomeArquivo, "jpg", diretorioArquivo)

        caminhoFoto = arquivoFoto.absolutePath

        return arquivoFoto
    }

    //cria uma intent implícita para exibir um endereço em um aplicativo de mapa
    fun mostraNoMapa() {
        val mostrarNoMapa = Intent(Intent.ACTION_VIEW)
        mostrarNoMapa.data = Uri.parse("geo:0,0?q=${edtEndereco.text}")

        if (mostrarNoMapa.resolveActivity(packageManager) != null) {
            startActivity(mostrarNoMapa)
        } else {
            Toast.makeText(this, "Impossível mostrar no mapa", Toast.LENGTH_SHORT).show()
        }
    }

    //cria uma intent implícita para abrir um aplicativo de e-mail
    fun enviaEmail() {
        val enviarEmail = Intent(Intent.ACTION_VIEW)
        enviarEmail.data = Uri.parse("mailto:${edtEmail.text}")
        enviarEmail.putExtra(Intent.EXTRA_SUBJECT, "Oi sumida")

        if (enviarEmail.resolveActivity(packageManager) != null) {
            startActivity(enviarEmail)
        } else {
            Toast.makeText(this, "Impossível enviar e-mail", Toast.LENGTH_SHORT).show()
        }
    }

    //exibe popup para selecionar entre enviar mensagem ou fazer ligação
    fun telefonaOuEnviaMensagem(view: View?) {
        val popup = PopupMenu(this, view)
        popup.inflate(R.menu.menu_telefona_ou_mensagem)

        popup.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.menuMensagem -> enviaMensagem()
                R.id.menuTelefona -> efetuaLigacao()
                else -> false
            }
        }

        popup.show()
    }

    //cria uma intent implícita para abrir um aplicativo de mensagens
    fun enviaMensagem():Boolean {
        val enviarMensagem = Intent(Intent.ACTION_VIEW)
        enviarMensagem.data = Uri.parse("sms:${edtTelefone.text}")
        enviarMensagem.putExtra("sms_body", "Oi sumida")

        if (enviarMensagem.resolveActivity(packageManager) != null) {
            startActivity(enviarMensagem)
            return true
        } else {
            Toast.makeText(this, "Impossível enviar mensagem", Toast.LENGTH_SHORT).show()
            return false
        }
    }

    //cria uma intent implícita para efetuar uma ligação e caso necessário solicita
    //permissão em tempo real para o usuário
    private fun efetuaLigacao(): Boolean {
        val efetuarLigacao = Intent(Intent.ACTION_CALL)
        efetuarLigacao.data = Uri.parse("tel:${edtTelefone.text}")

        //verifica se pode fazer ligação
        if (efetuarLigacao.resolveActivity(packageManager) != null) {

            //verifica se é necessário pedir permissão em tempo real
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                //verifica se tenho permissão para fazer ligação
                if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), REQUEST_PESMISSOES)
                    return false
                } else {
                    startActivity(efetuarLigacao)
                    return true
                }

            } else {
                startActivity(efetuarLigacao)
                return true
            }
        } else {
            Toast.makeText(this, "Impossível efetuar ligação", Toast.LENGTH_SHORT).show()
            return false
        }
    }

    //cria um objeto contatinho com os dados fornecidos e retorna seu nome
    //para a ListaContatinhoActivity
    private fun salvaContatinho() {

        //verifica se os campos obrigatórios estão preenchidos
        if(edtNome.text.isEmpty()){
            edtNome.requestFocus()
            edtNome.setError(getString(R.string.campo_obrigatorio))
            return
        }

        if(edtTelefone.text.isEmpty()){
            edtTelefone.requestFocus()
            edtTelefone.setError(getString(R.string.campo_obrigatorio))
            return
        }

        val contatinho = Contatinho(edtNome.text.toString(),
                edtTelefone.text.toString(),
                edtEmail.text.toString(),
                edtEndereco.text.toString(),
                caminhoFotoAceita)

        val abreLista = Intent(this, ListaContatinhosActivity::class.java)
        abreLista.putExtra(CONTATINHO, contatinho)
        setResult(Activity.RESULT_OK, abreLista)
        finish()

    }

    //exibe as informações do contatinho na Activity
    private fun carregaDados(contatinho: Contatinho) {
        edtNome.setText(contatinho.nome)
        edtTelefone.setText(contatinho.telefone)
        edtEmail.setText(contatinho.email)
        edtEndereco.setText(contatinho.endereco)

        GlideApp.with(this)
                .load(contatinho.caminhoFoto)
                .placeholder(R.drawable.ic_person)
                .centerCrop()
                .into(imgFoto)

        caminhoFotoAceita = contatinho.caminhoFoto

    }

}
